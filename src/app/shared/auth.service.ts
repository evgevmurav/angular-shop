import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {tap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(private http: HttpClient) {
    }

    login(user): any {
        return this.http.post(`https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${environment.apiKey}`, user)
            .pipe(
                tap((response) => this.setToken(response))
            );
    }

    private setToken(response): any {
        if (response) {
            const expData = new Date(new Date().getTime() + +response.expiresIn * 1000);
            localStorage.setItem('fb-token-exp', expData.toString());
            localStorage.setItem('fb-token', response.idToken);
        } else {
            localStorage.clear();
        }
    }

    get token(): any {
        const expDate = new Date(localStorage.getItem('fb-token-exp'));
        if (new Date() > expDate) {
            return null;
        }
        return localStorage.getItem('fb-token');
    }

    logout(): any {
        this.setToken(null);
    }

    isAuth(): any {
        return !!this.token;
    }
}
