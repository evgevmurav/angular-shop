import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProductService} from '../shared/product.service';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-dashboard-page',
    templateUrl: './dashboard-page.component.html',
    styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent implements OnInit, OnDestroy {

    constructor(private productServ: ProductService) {
    }

    products = [];
    pSub: Subscription;
    rSub: Subscription;

    ngOnInit(): void {
        this.pSub = this.productServ.getAll().subscribe(products => {
            this.products = products;
        });
    }

    ngOnDestroy(): void {
        if (this.pSub) {
            this.pSub.unsubscribe();
        }
        if (this.rSub) {
            this.rSub.unsubscribe();
        }
    }

    remove(id): any {
        this.rSub = this.productServ.remove(id).subscribe(() => {
            this.products = this.products.filter(product => product.id !== id);
        });
    }

}
