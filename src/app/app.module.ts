import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MainLayoutComponent } from './shared/main-layout/main-layout.component';
import { MainPageComponent } from './main-page/main-page.component';
import { ProductPageComponent } from './product-page/product-page.component';
import { CartPageComponent } from './cart-page/cart-page.component';
import { RouterModule } from '@angular/router';
import { AppRouting } from './app.routing';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {QuillModule} from 'ngx-quill';
import {AuthInterceptor} from './admin/shared/auth.interceptor';
import { ProductComponent } from './product/product.component';


@NgModule({
  declarations: [
    AppComponent,
    MainLayoutComponent,
    MainPageComponent,
    ProductPageComponent,
    CartPageComponent,
    ProductComponent
  ],
    imports: [
        BrowserModule,
        RouterModule,
        AppRouting,
        HttpClientModule,
        QuillModule.forRoot(),
    ],
  providers: [{
      provide: HTTP_INTERCEPTORS,
      multi: true,
      useClass: AuthInterceptor
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
